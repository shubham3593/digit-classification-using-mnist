#!/usr/bin/env python
# coding: utf-8

# # Digit Classification using MNIST
# #By- Shubham Kumar
# #Dated: June 17,2021

# In[1]:


import tensorflow as tf
import matplotlib.pyplot as plt


# In[2]:


mnist = tf.keras.datasets.mnist


# In[3]:


(x_train,y_train),(x_test,y_test) = mnist.load_data()


# In[4]:


plt.imshow(x_train[4],cmap=plt.cm.binary)
plt.show()


# In[5]:


x_train[0]


# In[6]:


x_train= tf.keras.utils.normalize(x_train,axis=1)
x_test= tf.keras.utils.normalize(x_test,axis=1)


# In[7]:


x_train[0]


# In[8]:


model=tf.keras.models.Sequential() #a feed forward model
model.add(tf.keras.layers.Flatten()) #takes our 28x28 and makes it 1x784
model.add(tf.keras.layers.Dense(128,activation=tf.nn.relu)) #a simple fully connected layer
model.add(tf.keras.layers.Dense(128,activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(10,activation=tf.nn.softmax)) # our output layer. 10 units for 10 classes. Softmax for probability distribution


# In[9]:


model.compile(optimizer='adam',
               loss='sparse_categorical_crossentropy', #how will we calculate the error to minimize the loss
              metrics=['accuracy'])
model.fit(x_train,y_train,epochs=10)


# In[10]:


val_loss,val_acc=model.evaluate(x_test,y_test)


# In[11]:


val_loss


# In[12]:


val_acc


# In[13]:


model.save(r'/home/aarush100616/Downloads/Projects/Digit Classification/digit_model.model')


# In[14]:


new_model=tf.keras.models.load_model(r'/home/aarush100616/Downloads/Projects/Digit Classification/digit_model.model')
predictions=new_model.predict(x_test)


# In[15]:


predictions[0]


# In[16]:


import numpy as np


# In[17]:


plt.imshow(x_test[30],cmap=plt.cm.binary)
plt.show()


# In[18]:


np.argmax(predictions[30])
